package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {
    @Autowired
    PostService postService;

    // create a new post
    @RequestMapping(value = "/posts", method = RequestMethod.POST)
    // "ResponseEntity" represents the whole HTTP response: status code, headers, and body
    public ResponseEntity<Object> createPost(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        //We can access the "postService" methods and pass the following arguments:
        // stringToken of the current session will be retrieved in the request headers.
        
        // a "post" object will be instantiated upon receiving the request body, this will follow the properties set in the model.
        // note: the "key" name of the request from postman should be similar to the property names set in the model.
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);
    }

    // Get all posts
    //s04 Activity:
    // Add a PostController method to retrieve all the posts from the database. This method should respond to a GET request at the /posts endpoint, and return a new response entity that calls the getPosts() method from postService.java. No arguments will be needed for this method.

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosts(){
        return new ResponseEntity<>(postService.getPosts(), HttpStatus.OK);
    }

    // Edit a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Post post){
        return postService.updatePost(postId, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value = "/posts/{postId}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value = "Authorization") String stringToken){
        return postService.deletePost(postId, stringToken);
    }

    @RequestMapping(value = "/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getMyPosts(@RequestHeader (value = "Authorization") String stringToken){
        return new ResponseEntity<>(postService.getMyPosts(stringToken), HttpStatus.OK);
    }

}
