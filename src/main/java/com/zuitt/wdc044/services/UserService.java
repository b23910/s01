package com.zuitt.wdc044.services;

// This interface will be used in the userController

import com.zuitt.wdc044.models.User;

import java.util.Map;
import java.util.Optional;

public interface UserService {
    // registration of a user
    void createUser(User user);

    // checking if user exists
    Optional<User> findByUsername(String username);

  //  Map<Object, Object> findByUsername(String username);
}


